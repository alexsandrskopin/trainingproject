<?php
/**
 * Created by PhpStorm.
 * User: alexsandrskopin
 * Date: 2020-02-11
 * Time: 11:38
 */

namespace Modules\Main;

use Phact\Module\Module;
use Modules\Admin\Traits\AdminTrait;

class MainModule extends Module
{
    use AdminTrait;
}
