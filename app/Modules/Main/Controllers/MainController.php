<?php
/**
 * Created by PhpStorm.
 * User: alexsandrskopin
 * Date: 2020-02-11
 * Time: 11:37
 */

namespace Modules\Main\Controllers;

use Phact\Controller\Controller;

class MainController extends Controller
{
    public function index()
    {
        echo $this->render('/pages/index/index.tpl');
    }
}